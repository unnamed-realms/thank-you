# 停服后文件与信息的供应服务 (Gitlab)

*某服 Minecraft S3服务器*


## .mcworld格式游戏存档

[Gitlab下载 \* \[UR.Final\]S3-230702.2112.1.19.73.02.zip.mcworld
](https://gitlab.com/unnamed-realms/thank-you/-/raw/main/files/v20230702/%5BUR.Final%5DS3-230702.2112.1.19.73.02.zip.mcworld?inline=false)


## 原始服务器程序和世界数据

[Gitlab下载 \* \[UR.Final\]S3-MinecraftServerProgramWithSaves.zip](https://gitlab.com/unnamed-realms/thank-you/-/raw/main/files/v20230702/%5BUR.Final%5DS3-MinecraftServerProgramWithSaves.zip?inline=false)


## Minecraft服务器软件

### 最新版本

见 https://www.minecraft.net/en-us/download/server/bedrock


### 1.19.73.02

| Windows | https://minecraft.azureedge.net/bin-win/bedrock-server-1.19.73.02.zip   |
|---------|-------------------------------------------------------------------------|
| Linux   | https://minecraft.azureedge.net/bin-linux/bedrock-server-1.19.73.02.zip |